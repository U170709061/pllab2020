#include <stdio.h>
#include <string.h>

#define MAX_LENGTH 80

void deletion(char * string, char * word);
int find(char *string, char *word);
void insertion(char* string, char* word, int position);

void deletion(char * string, char * word){

    int i, j;
    int a, b;
    int found;

    a = strlen(string);
    b = strlen(word);

    for(i = 0; i <= a-b; i++){

        found = 1;
        for(j = 0; j < b; j++){

            if(string[i + j] != word[j]){

                found = 0;
                break;

            }

            if(string[i+j] != ' ' && string[i+j] != '\t' && string[i+j] != 'n' && string[i+j] != '\0'){
                found = 0;
            }

            if(found == 1){

                for(j = i; j <= a - b; j++){

                    string[j] = string[j + b];

                }

                a = a - b;

                i--;
            }
        }
    }

    printf("New source is: %s", string);

}

int find(char *string, char *word){

    int i, len, f = 0;
    char c;
    word = c;
    len = strlen(string);
    for(i = 0; i<len; i++){
        if(string[i] == c){
            printf("Character position: %d", i+1);
            f = 1;

        }

    }



}

void insertion(char* string, char* word, int position){

    int a, b, n, i;
    a = strlen(string);
    b = strlen(word);
    n = position;

    for(i = n; i < a; i++){

        string[i+b] = string[i];
    }

    for(i = 0; i < b; i++){

        string[n + i] = word[i];
    }

    word[b+1] = '\0';

    printf("New source is: %s", word);


}

int main(){

    char string[MAX_LENGTH];
    char operation;
    char word[MAX_LENGTH];
    int position;
    int result;

    printf("Enter the source string: ");
    scanf("%s", &string);
    printf("Enter D(Delete), I(Insert), F(Find), or Q(Quit): ");
    scanf("%s", &operation);

   while(!(operation == 'D'|| operation == 'I' || operation == 'F' || operation == 'Q')){

        printf("Enter a valid operation: ");
        scanf("%s", &operation);

    }

    switch(operation){

    case 'D':
        printf("String to delete: ");
        scanf("%s", &word);
        deletion(string, word);
        break;

    case 'I':
        printf("String to insert: ");
        scanf("%s", &word);
        printf("Position of insertion: ");
        scanf("%d", &position);
        insertion(string, word, position);
        break;

    case 'F':
        printf("String to find: ");
        scanf("%s", &word);
        result = find(string, word);
        printf("%s found at position: %d", word, result);
        find(string, word);
        break;

    case 'Q':
        printf("%s", string);
        break;

    default:
        break;
    }



    return 0;

}
